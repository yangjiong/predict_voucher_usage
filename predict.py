import numpy as np
import csv
import config
import os
import pickle
import sklearn.externals.joblib
import matplotlib.pyplot as plt
from train import draw_feature_importances


def predict(model, feats):

    if model['scaler'] is not None:
        feats = model['scaler'].transform(feats)

    labels = model['model'].predict(feats)

    return labels


def predict_used_and_repurchase(model_used, model_repurchase, feats):

    labels_used = predict(model_used, feats)
    label_repurchase = predict(model_repurchase, feats)

    labels = np.vstack((labels_used, label_repurchase))

    return labels


def pack_labels_csv(fpath_csv_src, fpath_csv_dest, labels):

    with open(fpath_csv_src) as fsrc, open(fpath_csv_dest, 'w', newline='') as fdest:

        r = csv.reader(fsrc, delimiter=',')
        w = csv.writer(fdest, delimiter=',')

        # Check and remove header.
        row = next(r)
        assert row[0] == 'userid' and row[1] == 'promotionid_received' and \
               row[2] == 'voucher_code_received' and row[3] == 'voucher_received_time' and \
               row[4] == 'used' and row[5] == 'repurchase'
        w.writerow(row)

        ridx = 0
        for row in r:

            assert len(row[0]) > 0 and len(row[1]) > 0 and len(row[2]) > 0 and \
                   len(row[3]) > 0 and len(row[4]) == 0 and len(row[5]) == 0
            assert labels[ridx, 0] == int(row[0])

            assert labels[ridx, 1] == 0.0 or labels[ridx, 1] == 1.0
            assert labels[ridx, 2] == 0.0 or labels[ridx, 2] == 1.0

            row[4] = '0' if labels[ridx, 1] == 0.0 else '1'
            row[5] = '0' if labels[ridx, 2] == 0.0 else '1'

            w.writerow(row)

            ridx += 1


def print_feature_importance():

    dpath_data = config.config['dpath_data']

    fpath_model_used = os.path.join(dpath_data, 'model_used.pickle')
    print('---load used prediction model from', fpath_model_used, '\n')
    model_used = sklearn.externals.joblib.load(fpath_model_used)
    draw_feature_importances(model_used['model'], 'used')

    fpath_model_repurchase = os.path.join(dpath_data, 'model_repurchase.pickle')
    print('---load repurchase prediction model from', fpath_model_repurchase, '\n')
    model_repurchase = sklearn.externals.joblib.load(fpath_model_repurchase)
    draw_feature_importances(model_repurchase['model'], 'repurchase')

    plt.show()


def main():

    dpath_data = config.config['dpath_data']

    fpath_test_data = os.path.join(dpath_data, 'test_data.pickle')
    print('\n---load test data from', fpath_test_data)
    with open(fpath_test_data, 'rb') as f:
        uids, _, _, _, _ = pickle.load(f)
    print('%d test data entries loaded\n' % len(uids))

    fpath_test_feats = os.path.join(dpath_data, 'test_feats.npz')
    print('---load test feats from', fpath_test_feats)
    with np.load(fpath_test_feats) as test_feats:
        feats = test_feats['feats']
    print('%d test feature of dimension %d loaded\n' % feats.shape)

    fpath_model_used = os.path.join(dpath_data, 'model_used.pickle')
    print('---load used prediction model from', fpath_model_used, '\n')
    model_used = sklearn.externals.joblib.load(fpath_model_used)

    fpath_model_repurchase = os.path.join(dpath_data, 'model_repurchase.pickle')
    print('---load repurchase prediction model from', fpath_model_repurchase, '\n')
    model_repurchase = sklearn.externals.joblib.load(fpath_model_repurchase)

    print('---perform prediction')
    labels = predict_used_and_repurchase(model_used, model_repurchase, feats)
    labels = np.vstack((np.array(uids, dtype=np.float), labels))
    labels = np.swapaxes(labels, 0, 1)

    print('# of used:', np.sum(labels[:, 1]))
    print('# of repurchase:', np.sum(labels[:, 2]))
    print()

    fpath_test_csv = os.path.join(dpath_data, 'test.csv')
    fpath_test_csv_res = os.path.join(dpath_data, 'test_res.csv')
    print('---save predictions to', fpath_test_csv_res, '\n')
    pack_labels_csv(fpath_test_csv, fpath_test_csv_res, labels)


if __name__ == '__main__':

    main()
    # print_feature_importance()

