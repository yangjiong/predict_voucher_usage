import csv
import os
import pickle
import config
import datetime


def read_vouchers_csv(fpath_csv, train=True):
    """
    Load the training or testing samples from the csv files. Each voucher entry is a data sample.
    """

    data = []

    with open(fpath_csv) as f:

        r = csv.reader(f, delimiter=',')

        # Check and remove header.
        row = next(r)
        assert row[0] == 'userid' and row[1] == 'promotionid_received' and \
               row[2] == 'voucher_code_received' and row[3] == 'voucher_received_time' and \
               row[4] == 'used' and row[5] == 'repurchase'

        for row in r:

            # All fields should be filled for training data.
            # All fields except for 'used' or 'repurchased' should be filled for testing data.
            assert len(row[0]) > 0 and len(row[1]) > 0 and len(row[2]) > 0 and \
                   len(row[3]) > 0 and (not train or len(row[4]) > 0) and (not train or len(row[5]) > 0)

            data.append({'userid': int(row[0]),
                         'voucher_code_received': row[2],
                         'voucher_received_time': float(row[3]),
                         'used': bool(int(row[4])) if len(row[4]) > 0 else None,
                         'repurchase': bool(int(row[5])) if len(row[5]) > 0 else None})

    return data


def read_user_profiles_csv(fpath_csv):
    """
    Load the user profiles from the csv file.
    """

    data = []

    with open(fpath_csv) as f:

        r = csv.reader(f, delimiter=',')

        # Check and remove header.
        row = next(r)
        assert row[0] == 'userid' and row[1] == 'phone_verified' and \
               row[2] == 'email_verified' and row[3] == 'registration_time' and \
               row[4] == 'birthday' and row[5] == 'is_seller' and \
               row[6] == 'gender'

        for row in r:

            # All fields except for birthday and gender should be filled.
            assert len(row[0]) > 0 and len(row[1]) > 0 and len(row[2]) > 0 and \
                   len(row[3]) > 0 and len(row[5]) > 0

            data.append({'userid': int(row[0]),
                         'phone_verified': bool(int(row[1])),
                         'email_verified': bool(int(row[2])),
                         'registration_time':
                             datetime.datetime.strptime(row[3], '%Y-%m-%d %H:%M:%S') if len(row[3]) > 0 else None,
                         'birthday':
                             datetime.datetime.strptime(row[4], '%Y-%m-%d') if len(row[4]) > 0 else None,
                         'is_seller': bool(int(row[5])),
                         'gender': int(float(row[6])) if len(row[6]) > 0 else None})

    return data


def read_transactions_csv(fpath_csv):
    """
    Load the transactions from the csv file.
    """

    data = []

    with open(fpath_csv) as f:

        r = csv.reader(f, delimiter=',')

        # Check and remove header.
        row = next(r)
        assert row[0] == 'orderid' and row[1] == 'userid' and \
               row[2] == 'shopid' and row[3] == 'total_price' and \
               row[4] == 'order_time' and row[5] == 'voucher_code_used' and \
               row[6] == 'promotionid_used'

        for row in r:

            # All fields except for voucher_code_used should be filled.
            assert len(row[0]) > 0 and len(row[1]) > 0 and len(row[2]) > 0 and \
                   len(row[3]) > 0 and len(row[4]) > 0

            data.append({'orderid': int(row[0]),
                         'userid': int(row[1]),
                         'shopid': int(row[2]),
                         'total_price': float(row[3]),
                         'order_time': float(row[4]),
                         'voucher_code_used': row[5]})

    return data


def preprocess_data(vouchers, user_profiles, transactions):
    """
    Extract the relavent user profile and transactions associated with each voucher sample, and pack the data into a
    format that is easier for later analysis.
    """

    # user id for each training sample
    uids = [voucher['userid'] for voucher in vouchers]

    # label for each training sample
    labels = [[voucher['used'], voucher['repurchase']] for voucher in vouchers]

    # voucher information for each training sample
    vouchers = [{'voucher_code_received': voucher['voucher_code_received'],
                 'voucher_received_time': voucher['voucher_received_time']}
                for voucher in vouchers]

    # Retrieve the profile of the user for each voucher.
    user_profiles_dict = {}
    for user_profile in user_profiles:
        assert user_profile['userid'] not in user_profiles_dict
        user_profiles_dict[user_profile['userid']] = user_profile

    profiles = [user_profiles_dict[uid] for uid in uids]

    # Retrieve the transactions made by the user for each voucher before receiving the voucher.
    transactions_dict = {}
    for transaction in transactions:
        if transaction['userid'] in transactions_dict:
            transactions_dict[transaction['userid']].append(transaction)
        else:
            transactions_dict[transaction['userid']] = [transaction]

    trans = []
    for i in range(len(uids)):
        trans.append([transaction for transaction in transactions_dict[uids[i]] if
                      transaction['order_time'] <= vouchers[i]['voucher_received_time']])

    return uids, labels, vouchers, profiles, trans


def main():

    dpath_data = config.config['dpath_data']

    fpath_train = os.path.join(dpath_data, 'train.csv')
    print('\n---load training data from', fpath_train)
    trains = read_vouchers_csv(fpath_train, train=True)
    print('%d training samples loaded\n' % len(trains))

    fpath_test = os.path.join(dpath_data, 'test.csv')
    print('---load testing data from', fpath_test)
    tests = read_vouchers_csv(fpath_test, train=False)
    print('%d testing samples loaded\n' % len(tests))

    fpath_user_profiles = os.path.join(dpath_data, 'user_profiles.csv')
    print('---load user profiles from', fpath_user_profiles)
    user_profiles = read_user_profiles_csv(fpath_user_profiles)
    print('%d user profiles loaded\n' % len(user_profiles))

    fpath_transactions = os.path.join(dpath_data, 'transactions.csv')
    print('---load transactions from', fpath_transactions)
    transactions = read_transactions_csv(fpath_transactions)
    print('%d transactions loaded\n' % len(transactions))

    print('---preprocess training data\n')
    uids, labels, vouchers, profiles, trans = preprocess_data(trains, user_profiles, transactions)

    fpath_train_data = os.path.join(dpath_data, 'train_data.pickle')
    print('---save training data to', fpath_train_data, '\n')
    with open(fpath_train_data, 'wb') as f:
        pickle.dump([uids, labels, vouchers, profiles, trans], f)

    print('---preprocess testing data\n')
    uids, labels, vouchers, profiles, trans = preprocess_data(tests, user_profiles, transactions)

    fpath_test_data = os.path.join(dpath_data, 'test_data.pickle')
    print('---save testing data to', fpath_test_data, '\n')
    with open(fpath_test_data, 'wb') as f:
        pickle.dump([uids, labels, vouchers, profiles, trans], f)


if __name__ == '__main__':

    main()
