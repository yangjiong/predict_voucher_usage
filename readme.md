# Instructions to run the code for Shopee Mini Project

## Requirements

The code was tested on Python 3.4.3 (Ubuntu), Python 3.6.1 (Anaconda on Windows 7) and Python 3.6.1 (OS X).

The following python packages are needed to run the code.

	$ pip3 install numpy
	$ pip3 install scipy
	$ pip3 install matplotlib
	$ pip3 install scikit-learn

## Steps to Run the Code

1. Remove the '?' marks in the headers of 'train.csv' and 'test.csv'.

	* used? -> used
	* repurchase? -> repurchase
	
2. Modify *config.py* to set `config['dpath_data']` to the directory containing *train.csv*, *test.csv*, *transactions.csv* and *user_profiles.csv*.

3. Run the following python files in sequence.

		$ python3 input.py
		$ python3 features.py
		$ python3 train.py
		$ python3 predict.py
		
4. The prediction results will be saved to *test_res.csv*.

## More Information

The prediction results can be found [here](https://www.dropbox.com/s/qhgaarpg72n9sad/test_res.csv?dl=0).

A description on the algorithm can be found [here](https://www.dropbox.com/s/06o4ytfeewtusnl/tech_report.pdf?dl=0).

The cross-validation and training process of the random forest classifiers in *train.py* may take some time. The pretrained models can be downloaded [here](https://www.dropbox.com/s/88u94sgg6z521h4/models.zip?dl=0). Unzip the two pickle files and put them in `config['dpath_data']` to skip the running of *train.py*.

Some slides introducing the project can be downloaded here. [Keynote](https://www.dropbox.com/s/m06lwttuo4invoo/presentation.key?dl=0) or [PDF](https://www.dropbox.com/s/8gyvh4j6znhzwpt/presentation.pdf?dl=0)
