\documentclass[12pt]{article}

\usepackage{multirow}
\usepackage{caption}

\newcommand{\ie}{\textit{i.e.}}

\title{Technical Report for Shopee Mini Project}
\author{Jiong Yang}

\begin{document}

\maketitle

\section{Introduction}

An algorithm is developed to predict if a voucher given to an inactive user will be used and if the user will continue to repurchase after the voucher. The problem is formulated as two classification tasks in which a data sample is a user at a particular time stamp and the classification labels are the user's buying behaviors after a voucher is given at that time stamp, \ie, if the user will use the voucher and if the user will continue repurchasing after the voucher. It may look unnatural to also use the timestamp besides a user to denote a data sample but it is necessary because a user's intention to use a voucher or continue repurchasing varies from time to time. Since there are two labels for each sample, two prediction models are learned separately for the two classification tasks.

More specifically, Let subscript $i$ index a data sample, $u(i)$ and $t(i)$ denote the user and time stamp of sample $i$, respectively, $x_i\in\mathcal{R}^M$ denote the feature description of sample $i$ where $M$ is the dimensionality of the feature, $v_i\in\{0,1\}$ denote if user $u(i)$ uses the voucher, \ie, $v_i=1$ means user $u(i)$ uses the voucher while $v_i=0$ means user $u(i)$ does not use the voucher, and $r_i\in\{0,1\}$ denote if user $u(i)$ continue repurchasing after the voucher, \ie, $r_i=1$ means user $u(i)$ repurchases after the voucher while $r_i=0$ means user $u(i)$ does not repurchase after the voucher. We aim to learn two mappings, \ie, $f(x_i ){\to}v_i$ and $g(x_i){\to}r_i$, to predict if a voucher given to a user at a particular time stamp will be used and if the user will continue to repurchase after the voucher.

In the following, the detailed feature extraction process will be presented in Section \ref{sec:feature} and the classification model will be discussed in Section \ref{sec:prediction}.

\section{Feature Description} \label{sec:feature}

Two groups of features are extracted to describe each sample $i$, \ie, the user profile features of user $u(i)$ and the transaction features based on the transaction history of $u(i)$ before timestamp $t(i)$. The total feature dimension is 55. It is worth noting that we extract the transaction features only based on the transactions before $t(i)$ because, in the real-world scenario, the algorithm will be used to decide if a voucher will be given to a particular user and we only have the transaction history of the user up to now.

\subsection{User Profile Features}

The features extracted from user profiles are listed in Table 1. For the categorical features like ``Gender'', we use a binary stream to encode the categories. We do not use numeric values like $1.0$, $2.0$, $3.0$, and $4.0$ to directly encode different categories because there is no particular order between these categories. Furthermore, to handle the possible missing values in ``Biological Age'' and ``Gender'', we use an extra binary feature to encode the availability of this information. Overall, there are 11 features extracted based on the user profile.

\begin{table}
\footnotesize
\centering
\captionsetup{font=footnotesize}
\caption{Features based on user profiles}
\begin{tabular}{|c|c|l|} \hline
Features & Dimensionality & Values \\ \hline \hline
\multirow{2}{*}{Phone Verified} & \multirow{2}{*}{1} & $[0.0]$: not verified \\
& & $[1.0]$: verified \\ \hline \hline
\multirow{2}{*}{Email Verified} & \multirow{2}{*}{1} & $[0.0]$: not verified \\
& & $[1.0]$: verified \\ \hline \hline
\multirow{2}{*}{Biological Age} & \multirow{2}{*}{2} & $[0.0, -1.0]$: age not available \\ 
& & $[1.0, y]$: $y$ years old \\ \hline \hline
Membership Age & 1 & $[y]$: $y$ years old membership \\ \hline
\multirow{2}{*}{Is Seller} & \multirow{2}{*}{1} & $[0.0]$: is not seller \\
& & $[1.0]$: is seller \\ \hline \hline
\multirow{5}{*}{Gender} & \multirow{5}{*}{5} & $[1.0, 0.0, 0.0, 0.0, 0.0]$: gender not available \\ 
& & $[0.0, 1.0, 0.0, 0.0, 0.0]$: gender value is $1.0$ \\
& & $[0.0, 0.0, 1.0, 0.0, 0.0]$: gender value is $2.0$ \\
& & $[0.0, 0.0, 0.0, 1.0, 0.0]$: gender value is $3.0$ \\
& & $[0.0, 0.0, 0.0, 0.0, 1.0]$: gender value is $4.0$ \\ \hline
\end{tabular}
\end{table}

\subsection{Transaction Features}

The transaction features are extracted based on the past transaction history. There are two sub-groups of transaction features.

\subsubsection{Transaction Statistics}

We first extract the statistics about the historical transactions. We count the total number of transactions, the total price of the transactions, the total number of used vouchers, and the ratio between the number of transactions using voucher over the total number of transactions. To reflect the temporal proximity of the data to the time stamp, these statistics are counted over different time periods before $t(i)$, \ie, one week, two weeks, one month, three months, six months, one year, two years and infinite years.  These time periods are chosen based on the training data to ensure that each time period brings extra information. In total, there are $4\times8=32$ features.

\subsubsection{Histograms on Inactive Time Between Transactions}

Since the primary use case of the algorithm is to find the inactive users that are likely to become active again upon the receipt of voucher. We try to find if a user used to be brought from inactive to active by vouchers and if they continue repurchasing after the voucher usage. To extract these cues from past transactions, we build two histograms over the time period. The first histogram is over the time period between the transaction with a voucher and the previous transaction, \ie, the inactive period before a transaction with a voucher. The second histogram is over the time period between the transaction with a voucher and the next transaction, \ie, the inactive period after a transaction with a voucher. We discretize these continuous time periods into histograms, \ie, less than one week, two weeks, one month, three months, six months and one year, to reflect the length of the inactive time. In total, there are $6\times2=12$ features.

\section{Prediction Model} \label{sec:prediction}

Random forest classifiers are used to predict if a user will use the voucher and if the user will continue repurchasing after the voucher. Two prediction models are trained separately for the two prediction tasks. For each prediction model, 3-fold cross validation is performed on the training data to select the hyperparameters of the random forest estimator, \ie, the minimum number of samples in each leaf node, the total number of decision trees and the maximum number of features to sample for each decision tree. The F1 score is used as the evaluation criterion in the cross-validation process. In addition, it is observed that the numbers of training samples are highly unbalanced in different classes. A higher weight is assigned to the minority class in the training process. 

Besides the random forest estimator, linear SVM (support vector machine) and SVM with RBF (radial basis function) kernel are also tested in the experiment. However, it is found that the linear SVM estimator performs worse than the random forest estimator while the kernel SVM training process takes too long as we have more than 70K training samples. Note that, in the comparison with linear and kernel SVM, we do perform feature normalization as the scales of the feature dimensions affect their significance in the SVM model. Feature normalization is not needed for random forest estimator because the decision trees only need the absolute values of the features for branching.


\end{document}