import config
import os
import numpy as np
import sklearn
import sklearn.svm
import sklearn.dummy
import sklearn.model_selection
import sklearn.datasets
import sklearn.preprocessing
import sklearn.utils
import sklearn.ensemble
import matplotlib.pyplot as plt


def scale_feats(feats):
    """Scale each dimension of the features to be within [0, 1] and return the fitted scaler.
    """

    min_max_scaler = sklearn.preprocessing.MinMaxScaler()
    feats = min_max_scaler.fit_transform(feats)
    return feats, min_max_scaler


def train_dummy(feats, labels, **kwargs):

    dummy = sklearn.dummy.DummyClassifier()
    params = {'strategy': ['uniform']}

    dummy = train_helper(dummy, params, feats, labels, scoring=kwargs['scoring'], refit=kwargs['refit'],
                         cv=kwargs['cv'])

    return {'scaler': None, 'model': dummy}


def train_helper(estimator, params, feats, labels, **kwargs):

    print('%d samples of dimension %d to fit' % feats.shape)

    cv = sklearn.model_selection.GridSearchCV(estimator, params, n_jobs=-1, verbose=1, **kwargs)
    print(cv)
    cv.fit(feats, labels)

    print('\nbest parameter:', cv.best_params_, '\n')

    for score in kwargs['scoring']:
        print('<' + score + '>')
        for cvi in range(kwargs['cv']):
            kname = 'split%d_test_%s' % (cvi, score)
            print(kname + ':', cv.cv_results_[kname][cv.best_index_])

        kname = 'mean_test_%s' % score
        print(kname + ':', cv.cv_results_[kname][cv.best_index_], '\n')

    return cv.best_estimator_


def train_random_linear_svm(feats, labels, **kwargs):

    feats = np.random.rand(feats.shape[0], feats.shape[1])
    train_linear_svm(feats, labels, **kwargs)


def train_linear_svm(feats, labels, **kwargs):

    feats, scaler = scale_feats(feats)

    svm = sklearn.svm.LinearSVC(dual=False, class_weight=kwargs['class_weight'])
    params = {'C': [2.0 ** c for c in range(-10, 10, 2)]}

    svm = train_helper(svm, params, feats, labels, scoring=kwargs['scoring'], refit=kwargs['refit'], cv=kwargs['cv'])

    return {'scaler': scaler, 'model': svm}


def train_rbf_svm(feats, labels, **kwargs):

    feats, scaler = scale_feats(feats)

    svm = sklearn.svm.SVC(kernel='rbf', class_weight=kwargs['class_weight'])
    params = {'C': [2.0**c for c in range(-10, 10, 2)], 'gamma': [2.0**g for g in range(-10, 10, 2)]}

    svm = train_helper(svm, params, feats, labels, scoring=kwargs['scoring'], refit=kwargs['refit'], cv=kwargs['cv'])

    return {'scaler': scaler, 'model': svm}


def train_random_random_forest(feats, labels, **kwargs):

    feats = np.random.rand(feats.shape[0], feats.shape[1])
    train_random_forest(feats, labels, **kwargs)


def train_random_forest(feats, labels, **kwargs):

    rf = sklearn.ensemble.RandomForestClassifier(class_weight=kwargs['class_weight'])
    params = {'min_samples_leaf': [10, 50, 100, 200, 500], 'n_estimators': [10, 50, 100, 200, 500],
              'max_features': [10, 20, 30]}

    rf = train_helper(rf, params, feats, labels, scoring=kwargs['scoring'], refit=kwargs['refit'], cv=kwargs['cv'])

    return {'scaler': None, 'model': rf}


def draw_feature_importances(model, model_name):

    if hasattr(model, 'feature_importances_'):
        _, ax = plt.subplots()
        ax.bar(np.arange(model.feature_importances_.size) + 1.0, model.feature_importances_)
        ax.set_xlim([1, model.feature_importances_.size])
        ax.set_xlabel('feature dimension')
        ax.set_ylabel('feature importance')
        ax.set_title('feature importance plot of the %s model' % model_name)


def cross_validate_classifier(classifier, **kwargs):

    dpath_data = config.config['dpath_data']

    fpath_train_feats = os.path.join(dpath_data, 'train_feats.npz')
    print('\n---load training features from ', fpath_train_feats, '\n')
    with np.load(fpath_train_feats) as train_feats:
        feats = train_feats['feats']
        used_labels = train_feats['used_labels']
        repurchase_labels = train_feats['repurchase_labels']

    print('---train estimator for voucher usage')
    kwargs_ = kwargs.copy()
    model = classifier(feats, used_labels, **kwargs_)
    draw_feature_importances(model['model'], 'used')

    fpath_used_model = os.path.join(dpath_data, 'model_used.pickle')
    print('---save estimator to ', fpath_used_model, '\n')
    sklearn.externals.joblib.dump(model, fpath_used_model)

    print('---train estimator for repurchase')
    kwargs_ = kwargs.copy()
    model = classifier(feats, repurchase_labels, **kwargs_)
    draw_feature_importances(model['model'], 'repurchase')

    fpath_repurchase_model = os.path.join(dpath_data, 'model_repurchase.pickle')
    print('---save estimator to ', fpath_repurchase_model, '\n')
    sklearn.externals.joblib.dump(model, fpath_repurchase_model)

    plt.show()


def main():

    cross_validate_classifier(train_random_forest,
                              scoring=['average_precision', 'precision', 'recall', 'f1'], refit='f1', cv=3,
                              class_weight='balanced')


if __name__ == '__main__':

    main()
