import numpy as np
import datetime
import config
import os
import pickle


def extract_profile_features(voucher, profile):

    feat = list()

    # phone verified (1)
    feat.append(1.0 if profile['phone_verified'] else 0.0)

    # email verified (1)
    feat.append(1.0 if profile['email_verified'] else 0.0)

    # biological age (2)
    if profile['birthday'] is None:
        feat.extend([0.0, -1.0])
    else:
        age = datetime.datetime.fromtimestamp(voucher['voucher_received_time']).year - profile['birthday'].year
        feat.extend([1.0, age])

    # member age (1)
    age = datetime.datetime.fromtimestamp(voucher['voucher_received_time']).year - profile['registration_time'].year
    feat.append(age)

    # is seller (1)
    feat.append(1.0 if profile['is_seller'] else 0.0)

    # gender (5)
    if profile['gender'] is None:
        feat.extend([1.0, 0.0, 0.0, 0.0, 0.0])
    elif profile['gender'] == 1.0:
        feat.extend([0.0, 1.0, 0.0, 0.0, 0.0])
    elif profile['gender'] == 2.0:
        feat.extend([0.0, 0.0, 1.0, 0.0, 0.0])
    elif profile['gender'] == 3.0:
        feat.extend([0.0, 0.0, 0.0, 1.0, 0.0])
    elif profile['gender'] == 4.0:
        feat.extend([0.0, 0.0, 0.0, 0.0, 1.0])
    else:
        feat.extend([1.0, 0.0, 0.0, 0.0, 0.0])

    return feat


def extract_transaction_features(voucher, utrans):

    feat = list()

    # transaction statistics over different periods of times
    time_thres = [3600.0 * 24.0 * 7.0,  # one week
                  3600.0 * 24.0 * 7.0 * 2.0,  # two weeks
                  3600.0 * 24.0 * 7.0 * 4.0,  # one month
                  3600.0 * 24.0 * 7.0 * 4.0 * 3.0,  # three months
                  3600.0 * 24.0 * 7.0 * 4.0 * 6.0,  # six months
                  3600.0 * 24.0 * 7.0 * 4.0 * 12.0,  # one year
                  3600.0 * 24.0 * 7.0 * 4.0 * 12.0 * 2.0,  # two years
                  float('inf')]  # total

    numutrans = [0.0] * len(time_thres)
    totalprice = [0.0] * len(time_thres)
    numvouchers = [0.0] * len(time_thres)

    voucher_time = voucher['voucher_received_time']
    for utran in utrans:
        elapsed = voucher_time - utran['order_time']
        for ti in range(len(time_thres)):
            if elapsed < time_thres[ti]:
                numutrans[ti] += 1.0
                totalprice[ti] += utran['total_price']
                numvouchers[ti] += 1.0 if len(utran['voucher_code_used']) > 0 else 0.0

    voucher_use_ratios = [y / x if x > 0.0 else 0.0 for (x, y) in zip(numutrans, numvouchers)]

    feat.extend(numutrans + totalprice + numvouchers + voucher_use_ratios)

    # histogram of idle period before and after voucher usage
    sorted_utrans = sorted(utrans, key=lambda tran: tran['order_time'])
    time_thres = [3600.0 * 24.0 * 7.0,  # one week
                  3600.0 * 24.0 * 7.0 * 2.0,  # two weeks
                  3600.0 * 24.0 * 7.0 * 4.0,  # one month
                  3600.0 * 24.0 * 7.0 * 4.0 * 3.0,  # three months
                  3600.0 * 24.0 * 7.0 * 4.0 * 6.0,  # six months
                  3600.0 * 24.0 * 7.0 * 4.0 * 12.0]  # one year

    idletimes_before = [0.0] * len(time_thres)
    idletimes_after = [0.0] * len(time_thres)

    for ti in range(len(sorted_utrans)):
        if len(sorted_utrans[ti]['voucher_code_used']) > 0:

            if ti > 0:
                elapsed = sorted_utrans[ti]['order_time'] - sorted_utrans[ti - 1]['order_time']
                assert elapsed >= 0.0
                for tti in range(len(time_thres)):
                    if elapsed < time_thres[tti]:
                        idletimes_before[tti] += 1.0

            if ti < len(sorted_utrans) - 1:
                elapsed = sorted_utrans[ti + 1]['order_time'] - sorted_utrans[ti]['order_time']
                assert elapsed >= 0.0
                for tti in range(len(time_thres)):
                    if elapsed < time_thres[tti]:
                        idletimes_after[tti] += 1.0

    feat.extend(idletimes_before + idletimes_after)

    return feat


def extract_features(vouchers, profiles, trans):
    feats = [extract_profile_features(voucher, profile) +
             extract_transaction_features(voucher, utrans)
             for (voucher, profile, utrans) in zip(vouchers, profiles, trans)]

    return np.array(feats)


def extract_labels(labels):
    labels = np.array(labels, dtype=np.float)
    used_labels = labels[:, 0]
    repurchase_labels = labels[:, 1]

    return used_labels, repurchase_labels


def main():

    dpath_data = config.config['dpath_data']

    fpath_train_data = os.path.join(dpath_data, 'train_data.pickle')
    print('\n---load training data from', fpath_train_data, '\n')
    with open(fpath_train_data, 'rb') as f:
        _, labels, vouchers, profiles, trans = pickle.load(f)

        print('---extract training features')
        feats = extract_features(vouchers, profiles, trans)
        used_labels, repurchase_labels = extract_labels(labels)
        print('%d features of dimension %d extracted\n' % feats.shape)

        fpath_train_feats = os.path.join(dpath_data, 'train_feats.npz')
        print('---save training features to', fpath_train_feats, '\n')
        np.savez(fpath_train_feats, used_labels=used_labels, repurchase_labels=repurchase_labels, feats=feats)

    fpath_test_data = os.path.join(dpath_data, 'test_data.pickle')
    print('---load testing data from', fpath_test_data, '\n')
    with open(fpath_test_data, 'rb') as f:
        _, labels, vouchers, profiles, trans = pickle.load(f)

        print('---extract testing features')
        feats = extract_features(vouchers, profiles, trans)
        used_labels, repurchase_labels = extract_labels(labels)
        print('%d testing features of dimension %d extracted\n' % feats.shape)

        fpath_test_feats = os.path.join(dpath_data, 'test_feats.npz')
        print('---save testing features to', fpath_test_feats, '\n')
        np.savez(fpath_test_feats, used_labels=used_labels, repurchase_labels=repurchase_labels, feats=feats)


if __name__ == '__main__':

    main()
