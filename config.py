import platform

config = dict()

if platform.system() == 'Windows':
    config['dpath_data'] = r'D:\Playground\shopee_miniproj\data'
if platform.system() == 'Darwin':
    config['dpath_data'] = r'/Users/jiongyang/Playground/shopee_miniproj'
elif platform.system() == 'Linux':
    config['dpath_data'] = r'/home/jyang/SdbShared2TB/yang0374/playground/shopee_miniproj/data'
else:
    raise RuntimeError('The platform %s is not supported.' % platform.system())
